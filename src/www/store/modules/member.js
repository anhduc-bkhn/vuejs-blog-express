import api from '../../api';
import router from '../../router';
import { MEMBER_LOGIN, MEMBER_REGISTER, MEMBER_FORGETPWD, MEMBER_LOGOUT, MEMBER_INFO, REQUEST_AUTH, REQUEST_AUTH_SUCCESS } from '../const';

export default {
    state: {
        info: {},
        session: {},
        status: false,
        token: ''
    },
    mutations: {
        [MEMBER_INFO](state, data) {
            state.info = data;
        },

        [REQUEST_AUTH_SUCCESS](state, data) {
            if (data) {
                state.token = data.token
                state.session = data.session
                state.status = data.authenticated;
                localStorage.setItem("auth");
            } else {
                state.token = '';
                state.session = null;
                state.status = false;
                localStorage.removeItem("auth");
            }

        }
    },

    actions: {
        [REQUEST_AUTH](context, payload) {
            return api.post('/auth/session').then(res => {
                context.commit(REQUEST_AUTH_SUCCESS, res.body);
            })
        },
        [MEMBER_LOGIN]({ commit }, payload) {
            return api.post('/login', payload);
        },
        [MEMBER_INFO]({ commit }, payload) {
            api.memberInfo(payload).then(res => {
                commit(MEMBER_INFO, res.body);
            });
        },
        [MEMBER_REGISTER]({ commit }, payload) {
            return api.put('/register', payload);
        },
        [MEMBER_FORGETPWD]({ commit }, payload) {
            return api.post('/forgetpwd', payload);
        },

        [MEMBER_LOGOUT]({ commit }) {
            return api.get('/logout');
        },
    }
}
