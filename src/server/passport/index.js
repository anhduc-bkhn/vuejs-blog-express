// Invoke 'strict' JavaScript mode
'use strict';

// Load the module dependencies
var passport = require('passport'),
    Member = require('../models/member.js');

// Define the Passport configuration method
module.exports = function () {

    // Use Passport's 'serializeUser' method to serialize the user id
    passport.serializeUser(function (user, done) {
        done(null, user._id);
    });

    // Use Passport's 'deserializeUser' method to load the user document
    passport.deserializeUser(function (id, done) {
         Member.findOne({
            _id: id
        }, '-pwd', function (err, user) {
            done(err, user);
        });
        
        // member.findById(id , function (err, user) {

        //   // console.log(user);
        //     done(err, user);
        // });
    });

    // Load Passport's strategies configuration files
    require('./local.js')();
    //require('./passport/tumblr.js')();
    //require('./passport/twitter.js')();
    //require('./passport/facebook.js')();
    //require('./strategies/google.js')();
};
