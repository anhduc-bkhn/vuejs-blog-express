// Invoke 'strict' JavaScript mode
'use strict';
var passport = require('passport'),
    utils = require('mix-utils'),
    async = require('async'),
    crypto = require('crypto'),
    nodemailer = require('nodemailer'),
    sgTransport = require('nodemailer-sendgrid-transport'),
    moment = require('moment'),
    Member = require('../models/member');

var usermail = 'vinhnd@kaiocorp.com',
    auth = {
        auth: {
            api_key: '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'
        }
    },
    nodemailerSendgrid = nodemailer.createTransport(sgTransport(auth));

// Define the routes module' method
module.exports = app => {
    app.post('/member/login', function (req, res, next) {
        passport.authenticate('local', function (err, user, info) {

            if (err) {
                return next(err);
            }
            // if user is not found due to wrong username or password
            if (!user) {
                // console.log(info);
                res.json(info);
            } //(!user)
            //passport.js has a logIn user method
            else {
                req.logIn(user, function (err) {
                    if (err) { return next(err); }
                    if (req.body.rem == "1") {
                        req.session.cookie.maxAge = 2592000000; // 30*24*60*60*1000 Rememeber 'me' for 30 days
                    }
                    return res.json({
                        authenticated: true,
                        token: req.sessionID,
                        user: {
                            id: user._id,
                            uid: user.uid,
                            name: [user.fn, user.ln].join(' '),
                            fn: user.fn,
                            ln: user.ln,
                            mail: user.mail,
                            avt: user.avt,
                            cover: user.cver,
                            www: user.www,
                            bth: user.bth,
                            gder: user.gder,
                            bio: user.bio
                        }
                    });
                }); //req.logIn
            }
        })(req, res, next);
    });

    app.post('/member/register', function (req, res) {
        var _uid = req.body.mail;
        var _fn = req.body.fn;
        var _ln = req.body.ln;
        var _pwd = req.body.pwd;
        var _ip = utils.clientIp(req);
        var _ua = req.headers['user-agent'];
        if (_uid === "nguyendangvinh.dn@gmail.com") {
            var _role = 1 //admin
        } else { var _role = 0 }; //member
        var token;
        crypto.randomBytes(20, function (err, buffer) {
            token = buffer.toString('hex');
            return token;
        });
        var _x = [_fn, _ln, Math.floor((Math.random() * 9999) + 1)].join('.');

        Member.find({
            $or: [{
                'uid': _x
            }, {
                'mail': _uid
            }]
        }, function (err, user) {
            if (user && user.length > 0) {

                res.json({
                    msg: 'E-mail already exists'
                });

            } else {
                var _u = new Member({
                    uid: _x,
                    fn: _fn,
                    ln: _ln,
                    mail: _uid,
                    pwd: utils.sha256(_pwd),
                    role: _role,
                    act: {
                        ip: _ip,
                        ua: _ua
                    },
                    rpwt: token
                }).save(function (err, user) {

                    if (err)
                        return res.json(err);

                    return res.json({
                        msg: 'Successful registration, please confirm your email !'
                    });

                });

                var mailOptions = {
                    from: 'Kaio Social <' + usermail + '>',
                    to: _uid,
                    subject: 'Kaio Social confirm e-mail',
                    html: '<table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff"><tbody><tr>' +
                    '<td style="padding-top:30px;padding-bottom:30px;vertical-align:top;" valign="top">' +
                    '<table width="700" cellspacing="0" cellpadding="0" border="0" align="center"><tbody><tr>' +
                    '<td style="vertical-align:top;" valign="top">' +
                    '<table style="color:white;" width="700" cellspacing="0" cellpadding="0" border="0" bgcolor="#000000">' +
                    '<tbody><tr>' +
                    '<td style="vertical-align:top;" valign="top">' +
                    '<table width="700" cellspacing="0" cellpadding="0" border="0">' +
                    '<tbody><tr>' +
                    '<td style="vertical-align:top;" valign="top">' +
                    '<br></td></tr><tr>' +
                    '<td style="vertical-align:top;" valign="top"><br></td></tr><tr>' +
                    '<td style="background-color:#181818;" valign="top" bgcolor="#181818">' +
                    '<table width="700" cellspacing="0" cellpadding="0" border="0">' +
                    '<tbody><tr>' +
                    '<td style="vertical-align:top;background:#7e3f98;" valign="top">' +
                    '<table width="700" cellspacing="0" cellpadding="0" border="0"><tbody><tr>' +
                    '<td style="vertical-align:top;background:#181818;text-align:center;padding:5px 0;" valign="top">' +
                    '<a href="https://kaiosocial.com/" target="_blank"><img style="vertical-align:middle" width="150" src="http://i.imgur.com/urQfJii.png"></a>' +
                    '</td></tr><tr>' +
                    '<td style="vertical-align:top;background:#7e3f98;text-align:center;padding-top:30px;color:#fff;font-size:24px;line-height:40px;" valign="top">' +
                    '<h1 style="text-align:center;color:#fff;font-size:30px;">Verify Your Email Address !</h1>' +
                    '</td></tr><tr>' +
                    '<td style="vertical-align:top;background:#181818;" valign="top">' +
                    '<img alt="Welcome"width="700" height="360" src="http://i.imgur.com/VUEl6rp.jpg">' +
                    '</td></tr></tbody></table></td></tr><tr>' +
                    '<td style="color:#ffffff;padding:0 80px 20px 80px" valign="top"><table style="width:100%"><tbody><tr>' +
                    '<td style="padding-bottom:20px;" width="100%" valign="top">' +
                    '<p style="text-align:center" align="justify">' +
                    'To verify your email address ' + _uid + ' visit the following link:</h3>' +
                    '<h3 style="color:#ffffff;margin-top:0px;margin-bottom:20px;font-size:16px;font-weight:bold;line-height:22px;text-align:center;">' +
                    '<div style="margin-top:35px">' +
                    '<a href="http://' + req.headers.host + '/register/' + _uid + '/' + token + '" style="border: 1px solid;padding: 15px">Confirm your email address' +
                    '</a></div>' +
                    '</h3></td></tr></tbody></table></td></tr></tbody></table></td></tr><tr>' +
                    '<td style="vertical-align:top;" valign="top" bgcolor="#cfcac9">' +
                    '<table width="700" cellspacing="0" cellpadding="0" border="0"><tbody><tr>' +
                    '<td style="vertical-align:top;padding-top:15px;" valign="top" align="center">' +
                    '<img alt="logo" style="vertical-align:middle;" width="180" height="37" src="http://i.imgur.com/urQfJii.png">' +
                    '</td></tr><tr>' +
                    '<td style="color:#555156" valign="top" align="center"><p>Thanks for signing up, and we hope you enjoy our service.</p><p>Follow us on Facebook and Twitter</p></td>' +
                    '</tr><tr><td valign="top" align="center">' +
                    '<table cellspacing="0" cellpadding="0" border="0" align="center"><tbody><tr>' +
                    '<td style="padding:0 5px">' +
                    '<a href="https://www.facebook.com/Kaioteam" target="_blank">' +
                    '<img alt="Facebook" src="http://zcdnr.zapak.com/www/zapak2015/images/facebookIcon.png">' +
                    '</a></td>' +
                    '<td style="padding:0 5px">' +
                    '<a href="https://twitter.com/Kaioteam" target="_blank">' +
                    '<img alt="Twitter" src="https://pbs.twimg.com/profile_images/767879603977191425/29zfZY6I.jpg" width="46px">' +
                    '</a></td></tr></tbody></table></td></tr><tr>' +
                    '<td style="padding:10px 0;color:#555156;text-align:center;font-size:12px;" valign="top" align="center">Copyright Â© 2016 KaioSocial, Inc. All rights reserved.</td>' +
                    '</tr><tr>' +
                    '<td style="padding: 10px;line-height:20px;color:#555156;font-size:12px;" valign="top" align="center">You are receiving this email because an account was created on KaioSocial with this email address. If you are the owner of this email address and did not create the account, just ignore this message and the account will remain inactive.</td>' +
                    '</tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table>'
                };

                nodemailerSendgrid.sendMail(mailOptions, function (error, info) {
                    if (error) {
                        console.log(error);
                    } else {
                        console.log('message send: ' + info.response);
                    }
                });

            }
        });

    });

    app.get('/register/:mail/:token', function (req, res) {
        var _mail = req.params.mail;
        var token = req.params.token;

        Member.update({ mail: _mail, rpwt: token }, { $set: { stt: 1, rpwt: "" } }, function (err) {
            if (err) {
                console.log(err);
            }
            res.redirect('/login');
        });
    });

    app.post('/member/forgetpwd', function (req, res) {
        var _uid = req.body.mail;

        async.waterfall([
            function (done) {
                crypto.randomBytes(20, function (err, buf) {
                    var token = buf.toString('hex');
                    done(err, token);
                });
            },
            function (token, done) {
                Member.findOne({
                    $or: [{
                        'uid': _uid
                    }, {
                        'mail': _uid
                    }]
                }, function (err, user) {
                    if (!user) {
                        res.json({
                            msg: 'This E-mail is not registered !'
                        });
                    } else {

                        user.rpwt = token;
                        user.rpwe = Date.now() + 3600000; // 1 hours

                        user.save(function (err) {
                            done(err, token, user);
                        });
                    }
                });
            },
            function (token, user, done) {

                var mailOptions = {
                    from: 'Kaio Social <' + usermail + '>',
                    to: _uid,
                    subject: 'Kaio Social Password Reset',
                    text: 'You are receiving this because you (or someone else) have requested the reset of the password for your account.\n\n' +
                    'Please click on the following link, or paste this into your browser to complete the process, 1 hours following this link will not be used:\n\n' +
                    'http://' + req.headers.host + '/forgotpwd/' + token + '\n\n' +
                    'After clicking on link, please check the mail to get new password \n\n If you did not request this, please ignore this email and your password will remain unchanged.\n'
                };

                nodemailerSendgrid.sendMail(mailOptions, function (error, info) {
                    if (error) {
                        console.log(error);
                        return res.json({ msg: 'The system is faulty, please come back in a few minutes !' });
                    } else {
                        console.log('message send: ' + info.response);
                        return res.json({ msg: 'Send mail completed , Please check mail !' });
                    }
                });
            }
        ], function (err) {
            if (err) return next(err);
            res.redirect('/forgotpwd');
        });
    });

    app.get('/forgotpwd/:token', function (req, res) {
        var _token = req.params.token,
            x = Math.floor((Math.random() * 9999) + 1),
            _npwd = utils.sha256('mail@' + x);

        Member.findOne({ rpwt: _token }, function (err, doc) {

            var mailOptions = {
                from: 'Kaio Social <' + usermail + '>',
                to: doc.mail,
                subject: 'Your New Password',
                html: '<h2><b>Your new password is : mail@' + x + '</b></h2>'
            };
            nodemailerSendgrid.sendMail(mailOptions, function (error, info) {
                if (error) {
                    console.log(error);
                } else {
                    console.log('message send: ' + info.response);
                }
            });
        });

        Member.update({ rpwt: _token, rpwe: { $gt: Date.now() } }, { $set: { pwd: _npwd, rpwt: '' } }, function (err, doc) {
            if (err) {
                console.log(err);
                return res.json({ msg: err.message });
            }
            res.redirect('/login');
        });
    });

    app.post('/resend', function (req, res) {
        var _uid = req.body.uid;
        async.waterfall([
            function (done) {
                crypto.randomBytes(20, function (err, buf) {
                    var token = buf.toString('hex');
                    done(err, token);
                });
            },
            function (token, done) {
                Member.findOne({
                    $or: [{
                        'uid': _uid
                    }, {
                        'mail': _uid
                    }]
                }, function (err, user) {
                    if (!user) {
                        res.json({
                            msg: 'This E-mail is not registered !'
                        });
                    } else {

                        user.rpwt = token;
                        user.rpwe = Date.now() + 3600000; // 1 hours

                        user.save(function (err) {
                            done(err, token, user);
                        });
                    }
                });
            },
            function (token, user, done) {

                var mailOptions = {
                    from: 'Kaio Social <' + usermail + '>',
                    to: _uid,
                    subject: 'Kaio Social confirm e-mail',
                    html: '<table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff"><tbody><tr>' +
                    '<td style="padding-top:30px;padding-bottom:30px;vertical-align:top;" valign="top">' +
                    '<table width="700" cellspacing="0" cellpadding="0" border="0" align="center"><tbody><tr>' +
                    '<td style="vertical-align:top;" valign="top">' +
                    '<table style="color:white;" width="700" cellspacing="0" cellpadding="0" border="0" bgcolor="#000000">' +
                    '<tbody><tr>' +
                    '<td style="vertical-align:top;" valign="top">' +
                    '<table width="700" cellspacing="0" cellpadding="0" border="0">' +
                    '<tbody><tr>' +
                    '<td style="vertical-align:top;" valign="top">' +
                    '<br></td></tr><tr>' +
                    '<td style="vertical-align:top;" valign="top"><br></td></tr><tr>' +
                    '<td style="background-color:#181818;" valign="top" bgcolor="#181818">' +
                    '<table width="700" cellspacing="0" cellpadding="0" border="0">' +
                    '<tbody><tr>' +
                    '<td style="vertical-align:top;background:#7e3f98;" valign="top">' +
                    '<table width="700" cellspacing="0" cellpadding="0" border="0"><tbody><tr>' +
                    '<td style="vertical-align:top;background:#181818;text-align:center;padding:5px 0;" valign="top">' +
                    '<a href="https://kaiosocial.com/" target="_blank"><img style="vertical-align:middle" width="150" src="http://i.imgur.com/urQfJii.png"></a>' +
                    '</td></tr><tr>' +
                    '<td style="vertical-align:top;background:#7e3f98;text-align:center;padding-top:30px;color:#fff;font-size:24px;line-height:40px;" valign="top">' +
                    '<h1 style="text-align:center;color:#fff;font-size:30px;">Verify Your Email Address !</h1>' +
                    '</td></tr><tr>' +
                    '<td style="vertical-align:top;background:#181818;" valign="top">' +
                    '<img alt="Welcome"width="700" height="360" src="http://i.imgur.com/VUEl6rp.jpg">' +
                    '</td></tr></tbody></table></td></tr><tr>' +
                    '<td style="color:#ffffff;padding:0 80px 20px 80px" valign="top"><table style="width:100%"><tbody><tr>' +
                    '<td style="padding-bottom:20px;" width="100%" valign="top">' +
                    '<p style="text-align:center" align="justify">' +
                    'To verify your email address ' + _uid + ' visit the following link:</h3>' +
                    '<h3 style="color:#ffffff;margin-top:0px;margin-bottom:20px;font-size:16px;font-weight:bold;line-height:22px;text-align:center;">' +
                    '<div style="margin-top:35px">' +
                    '<a href="http://' + req.headers.host + '/register/' + _uid + '/' + token + '" style="border: 1px solid;padding: 15px">Confirm your email address' +
                    '</a></div>' +
                    '</h3></td></tr></tbody></table></td></tr></tbody></table></td></tr><tr>' +
                    '<td style="vertical-align:top;" valign="top" bgcolor="#cfcac9">' +
                    '<table width="700" cellspacing="0" cellpadding="0" border="0"><tbody><tr>' +
                    '<td style="vertical-align:top;padding-top:15px;" valign="top" align="center">' +
                    '<img alt="logo" style="vertical-align:middle;" width="180" height="37" src="http://i.imgur.com/urQfJii.png">' +
                    '</td></tr><tr>' +
                    '<td style="color:#555156" valign="top" align="center"><p>Thanks for signing up, and we hope you enjoy our service.</p><p>Follow us on Facebook and Twitter</p></td>' +
                    '</tr><tr><td valign="top" align="center">' +
                    '<table cellspacing="0" cellpadding="0" border="0" align="center"><tbody><tr>' +
                    '<td style="padding:0 5px">' +
                    '<a href="https://www.facebook.com/Kaioteam" target="_blank">' +
                    '<img alt="Facebook" src="http://zcdnr.zapak.com/www/zapak2015/images/facebookIcon.png">' +
                    '</a></td>' +
                    '<td style="padding:0 5px">' +
                    '<a href="https://twitter.com/Kaioteam" target="_blank">' +
                    '<img alt="Twitter" src="https://pbs.twimg.com/profile_images/767879603977191425/29zfZY6I.jpg" width="46px">' +
                    '</a></td></tr></tbody></table></td></tr><tr>' +
                    '<td style="padding:10px 0;color:#555156;text-align:center;font-size:12px;" valign="top" align="center">Copyright Â© 2016 KaioSocial, Inc. All rights reserved.</td>' +
                    '</tr><tr>' +
                    '<td style="padding: 10px;line-height:20px;color:#555156;font-size:12px;" valign="top" align="center">You are receiving this email because an account was created on KaioSocial with this email address. If you are the owner of this email address and did not create the account, just ignore this message and the account will remain inactive.</td>' +
                    '</tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table>'
                };
                nodemailerSendgrid.sendMail(mailOptions, function (error, info) {
                    if (error) {
                        console.log(error);
                    } else {
                        console.log('message send: ' + info.response);
                        return res.json({ msg: 'Send mail completed , Please check mail !' });
                    }
                });
            }
        ], function (err) {
            if (err) return next(err);
            res.redirect('/login');
        });

    });

    app.post('/changepwd', function (req, res) {
        var _id = req.user._id,
            opwd = req.body.opwd,
            npwd = req.body.npwd
        // console.log(utils.sha256(opwd));
        Member.findOne({ _id: _id }, function (err, doc) {
            if (utils.sha256(opwd) != doc.pwd) {
                return res.json({
                    msg: 'Old password incorrect !'
                });
            } else {
                Member.findOneAndUpdate({ _id: _id }, { $set: { "pwd": utils.sha256(npwd) } }, function (err, doc) {
                    if (err) {
                        console.log(err);
                        return res.json({ msg: err.message });
                    }

                    return res.json({ msg: 'Save successfully.' });
                });
            }
        });
    });

    app.get('/logout', function (req, res) {
        req.session.destroy();
        req.logout();
    });

}